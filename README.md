A tool to find ip addresses and mac addresses on a directly-connected network range, and create kea dhcp reservations out of them

To run you need the jc library installed:
```
~/g/netscan> pip3 install --user jc
```

Run like this:
```
~/g/netscan> python3 netscan.py 10.1.0.0/23
{
  # 2020-05-14 - jtuckey scan - mac: 2c:f4:32:d3:b6:16 - reversedns: arlec-pb-tv.lan
  "hw-address": "2c:f4:32:d3:b6:16",
  "hostname": "arlec-pb-tv.lan",
  "ip-address": "10.1.0.10",
},
{
  # 2020-05-14 - jtuckey scan - mac: 30:fd:38:c9:a2:29 - reversedns: livingroom-chromecast.lan
  "hw-address": "30:fd:38:c9:a2:29",
  "hostname": "livingroom-chromecast.lan",
  "ip-address": "10.1.0.12",
},
{
  # 2020-05-14 - jtuckey scan - mac: 78:0f:77:00:5a:67 - reversedns: livingroom-rmmini.lan
  "hw-address": "78:0f:77:00:5a:67",
  "hostname": "livingroom-rmmini.lan",
  "ip-address": "10.1.0.13",
},
{
  # 2020-05-14 - jtuckey scan - mac: a0:20:a6:01:6f:f9 - reversedns: dhtboy1.lan
  "hw-address": "a0:20:a6:01:6f:f9",
  "hostname": "dhtboy1.lan",
  "ip-address": "10.1.0.14",
},
```
Don't forget to remove the final comma from the output. :)