#!/usr/bin/python3

import ipaddress
import jc.parsers.arp
import subprocess
import argparse
import socket
import logging
import re
from logging import debug

# logging.getLogger().setLevel(logging.DEBUG)
date = '2020-05-14'
name = 'jtuckey'

def main():
    parser = argparse.ArgumentParser('netscan')
    parser.add_argument('net')
    args = parser.parse_args()

    debug(f"running with args: {args}")
    net = ipaddress.ip_network(args.net)
    id_counter = 0

    for num, ip in enumerate(net):
        if num <= 9:
            continue
        if num == (net.num_addresses -1):
            break  # don't try to process the broadcast address

        
        
        debug(f"ip: {str(ip)}")
        pingproc = subprocess.run(['ping', '-w', '1', '-c', '1', str(ip)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=False)
        try:
            debug(f"getting arp entry")
            arpproc = subprocess.run(['arp', str(ip)], stdout=subprocess.PIPE, check=True)
            debug(f"{arpproc.stdout.decode()}")
            arp = jc.parsers.arp.parse(arpproc.stdout.decode())
            arpresult = arp[0]['hwaddress']
            if not re.match(r'^(\w\w:){5}\w\w$', arpresult):
                arpresult = ''
        except:
            arpresult = ''
        try:
            reverseresult = socket.gethostbyaddr(str(ip))[0]
        except:
            reverseresult = ''

        debug(f"ping: {pingproc.returncode}, arp: {arpresult}, reverse: {reverseresult}")


        if pingproc.returncode == 0 or arpresult or reverseresult:
            # Create a reservation

            string = f'''{{
  # {date} - {name} scan - mac: {arpresult} - reversedns: {reverseresult}
'''
            if not arpresult:
                # Create a fake mac for the device
                id_counter += 1
                arpresult = f"01:01:01:01:01:{id_counter:02X}"

            string += f'''  "hw-address": "{arpresult}",
'''
            if reverseresult:
                string += f'''  "hostname": "{reverseresult}",
'''
            string += f'''  "ip-address": "{ip}",
}},'''

            print(string)


if __name__ == "__main__":
    main()




